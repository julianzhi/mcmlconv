# Hopefully simple converter for MultiCellml
##
run with MCmlconv -i Cell_Sorting_MultiCellML_v0.2.3.xml -s MCml -o test.xml -t Morph -l log.xml
or just  MCmlconv -i Cell_Sorting_MultiCellML_v0.2.3.xml -s MCml -o test.xml -t Morph
## Idea
I try to make very light back end for the conversion rules.
So that it is hopefully easy to write the rules and the back end will keep
track of used information. For now it only works with xml sources. 

Each conversions rules are in a separate python file named source_2_target.py. 
It must contain:
- rules_list      : source_2_target

The rules list should have a format like:   
[(rule_function1,[(key1,message1),(key2,message2)]),     
 (rule_function2,[(key10,message10),(key11,message11)])]

The keys and messages are used in the status_dict which is passed to the validate method of the 
source object. 

It helps a lot to use decorators to creates this list.
See my example decorator_generator, which is usefully for xml files. 
There the keyn is the xpath to the node and the message tells if the rules uses the node.

Each rule takes a source object and a target object as argument.
These objects should be defined in base.py.  

Functions useful for different conversions and rules have a home in tools.py

In cli.py is the command line interface.


## Depends
- lxml
- click

## Todo:
 - Check if it makes sense
 - Test with other formats
 - Maybe extend to other inputs
 -
 - Write rules
