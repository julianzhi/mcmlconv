import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="MCmlconv",
    version="0.0.1",
    author="Julian Rode",
    author_email="julian.rode1@tu-dresden.de",
    description="",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=['click','lxml'
                      ],
    entry_points={
        'console_scripts': [
            'MCmlconv = MCmlconv.cli:MCmlconvert',
        ],
    },
)
