import lxml.etree as et


xsl_str='''<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output indent="yes"/>
  <xsl:strip-space elements="*"/>

  <xsl:param name="orig_namespace"/>
  <xsl:param name="new_namespace"/>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="*" priority="1">
    <xsl:choose>
      <xsl:when test="namespace-uri()=$orig_namespace">
        <xsl:element name="{name()}" namespace="{$new_namespace}">
          <xsl:apply-templates select="@*|node()"/>
        </xsl:element>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>'''
xslt = et.fromstring(xsl_str)
def transform_ns(element,orig_namespace,new_namespace):
    """transform namespace of element or tree.
    This function changes the imput!
    
    Parameters
    ----------
   
    element : lxml element
      change namespace of element and its children.
      will be change.

    orig_namespace : string
      name of the original namespace
    
    new_namespace : string
      name of the new namespace

    Returns
    ------- 

    element : lxml element
      returns a lxml element with the new namespace
    """
    return et.XSLT(xslt)(element,orig_namespace="'{}'".format(orig_namespace),\
        new_namespace="'{}'".format(new_namespace))  

