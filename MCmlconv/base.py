import lxml.etree as et
from copy import deepcopy
import logging
logger = logging.getLogger(__name__)

class RuleError(Exception):
   """Raised when rule failes complete"""
   pass

class PartialRuleError(Exception):
   """Raised when rule failes complete partly"""
   pass

def decorator_generator(list_of_rules,namespace_definitions):
    '''
    Generate a decorator for the rules for a conversion e.g. MultiCellml to Morpheus
    Example list_of_rules:
    [(rule_function1, [('xpath to element','processing typ'),\
                        'Model/Description','Complete']),\
    [(rule_function2, [...])]]


    Processing types
        Complete : the node and it child are assumed to be translated
        ....  : ....

    Parameters
    ----------
    list_of_rules : list
        name of the list to save the rules and nodes affected by the rule

    namespace_definitions : 
        namespace definitions for the source object e.g. MultiCellml

    Returns
    -------
    decorator function : function
        the decorator writes the function and the nodes which are processed by the rule
        in the list_of_rules 

    '''
    def decorator(list_of_tags):
        def wrap(f):
            node_list = []
            for tag in list_of_tags:
                path = []
                for tag_path in tag[0].split('/'):
                    if tag_path.find(':')<0:
                        tag_path_element = [None,tag_path]
                    else:
                        tag_path_element = tag_path.split(':')
                    path.append( '{{{}}}{}'.format(\
                        namespace_definitions[tag_path_element[0]],\
                        tag_path_element[1])\
                        )
                node_list.append(('/'.join(path),tag[1]))
            list_of_rules.append( (f,node_list)  )
            return f
        return wrap
    return decorator


'''
Base object for the sources and target
I think it should be possible to use one class for source and target.
If source it is called with path and if it is target without path
The classes must have a write function to generate the output.
The rest just have to work in the custom rules.
'''
class xml_base:
    def write(self,path):
        with open(path, 'wb') as f:
            f.write(et.tostring(self.root, pretty_print=True))

    def __repr__(self):
        return et.tostring(self.root, pretty_print=True).decode("utf-8")

    def validate(self,status_dict,logpath=None):
        if logpath is None:
            walker = et.iterwalk(self.tree, events=("start", "end"))
        else:
            xml_anot = deepcopy(self.tree)        
            walker = et.iterwalk(xml_anot, events=("start", "end"))
        while (True):
            try:
                event,element = next(walker)
            except StopIteration:
                break
            if (event=='start'):
                try:
                    if logpath is None:
                        tag_path = self.tree.getelementpath(element)
                    else:
                        tag_path = xml_anot.getelementpath(element)
                    tag_path = re.sub('\[[0-9]+\]','',tag_path)  #ingnore of tag numbers 
                    status = status_dict[tag_path]
                    print(status)
                    if 'Complete' in status:
                        if logpath is not None:
                            element.set('ConverterStatus','Complete')
                        walker.skip_subtree()
                    else:
                        if logpath is None:
                            logger.warning('Element error in: ' + self.tree.getelementpath(element) + ' ' +  ','.join(status))
                        else:
                            element.set('ConverterStatus',','.join(status))
                        
                        
                except:
                    if logpath is None:
                        logger.warning('Element missing rule: ' + self.tree.getelementpath(element) )
                    else:
                        element.set('ConverterStatus','Element missing rule')

        if logpath is not None:
            with open(logpath, 'wb') as f:
                f.write(et.tostring(xml_anot, pretty_print=True))
'''
MultiCellml base object and namespace
'''
class MCml_obj(xml_base):
    ns = {None:"URL-to-specs","cpm":"CPM","vertex":"vertex"} 
    def __init__(self,path=None):
        if path is None:
            self.root = et.Element("MultiCellML",\
                attrib={'version':'0.2.3'},nsmap=self.ns)
        else:
            self.tree = et.parse(path)
            self.root =  self.tree.getroot()
            self.model = self.root.find('Model',namespaces=self.ns)
            self.sim = self.root.find('Model',namespaces=self.ns)   

'''
Morpheus base object and namespace
''' 
class Morph_obj(xml_base):
    ns = {None:"URL-Morpheus"}
    def __init__(self,path=None):
        if path is None:
            self.root = et.Element("MorpheusModel"\
                ,attrib={'version':str(5)},nsmap=self.ns)
        else:
            self.tree = et.parse(path)
            self.root =  self.tree.getroot()





