import lxml.etree as et
from copy import copy
from .base import decorator_generator
from .base import MCml_obj,Morph_obj
from .tools import transform_ns

MCml_2_Morph_rules = []
MCml_2_Morph = decorator_generator(MCml_2_Morph_rules,MCml_obj.ns)

@MCml_2_Morph([('Model/Description','Complete'),\
               ('Model/Description2','Complete')])
def write_description(MCml,Morph):
    desc = copy(MCml.model.find('Description',namespaces=MCml.ns))
    desc.tail = None
    Morph.root.append( transform_ns(desc,\
                orig_namespace=MCml.ns[None],\
                new_namespace=Morph.ns[None]\
                    ).getroot())

