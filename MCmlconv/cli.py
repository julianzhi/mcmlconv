import click
from .base import *
from .MCml_2_Morph import MCml_2_Morph_rules
#dict of supported conversions (source,dict of targets)
conversion_dict = {'MCml':(MCml_obj, {\
                            'Morph':[Morph_obj,MCml_2_Morph_rules]\
                                     })}

help_format = 'supported conversion:\n'
for name,conv in conversion_dict.items():
    help_format += '\tFrom ' + name + ' to '\
         + ', '.join([name for name,_ in conv[1].items()]) + '\n'
    

@click.command(help=help_format)
@click.option('--input_file','-i', help='Name of input file.')
@click.option('--input_typ', '-s',  help='Type of input file.')
@click.option('--output_file', '-o',  help='Name of output file.')
@click.option('--output_typ', '-t', help='Type of output file.')
@click.option('--log_file', '-l', help='Name of log file not needed.')
def MCmlconvert(input_file, input_typ, output_file, output_typ,log_file):
    """
    Main convert function 
    read the creates the input and output objects from the definitions
    in conversion_dict.
    Then it runs the rules and populates the status dict.

    """
    try:
        input_obj,in_dict = conversion_dict[input_typ]
        try:
            output_obj,rules = in_dict[output_typ]
        except:
            print('outout',output_typ,'not supported')
    except:
        print('input',input_typ,'not supported')
    
    in_obj = input_obj(input_file)
    out_obj = output_obj()
    status_dict = dict()
    for func,meta in rules:
        status = None
        try:
            func(in_obj,out_obj) 
        except RuleError as e:
            status = 'Error'
            logger.info('Rule Error' + str(e))
        except PartialRuleError as e:
            status = 'Partial Error'
            logger.info('Parial Rule Error' + str(e))
        for tag in meta:
            try:
                old = status_dict[tag[0]]
                if status is not None:
                    old.append(status)
                old.append(tag[1])
            except KeyError:
                if status is not None:
                    tmp = [status,tag[1]]
                else:
                    tmp = [tag[1]]
                status_dict[tag[0]] = tmp
    in_obj.validate(status_dict,log_file)
    out_obj.write(output_file)

if __name__ == '__main__':
    MCmlconvert()